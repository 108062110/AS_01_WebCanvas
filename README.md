# AS_01_WebCanvas

# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.
    預設為筆刷，點擊橡皮擦可以清除筆刷軌跡。
    UNDO是上一步，REDO是恢復，CLEAR重整頁面
    有常用顏色及一個色盤
    下方欄位有拉條可以調整筆刷跟橡皮擦粗細，文字功能(輸入完文字請按"ENTER")、下載、上傳、及基本的圖形方塊

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"
    https://108062110.gitlab.io/AS_01_WebCanvas/
### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
