const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
canvas.width = window.innerWidth - 100;
canvas.height = window.innerHeight - 100;
//顏色
//寬度
ctx.lineWidth = 13;
//形狀
ctx.lineCap = 'round';
ctx.lineJoin = 'round';
var x1 = 0;
var y1 = 0;
var x2 = 0;
var y2 = 0;
var text = '';
var textX;
var textY;
var start_x;
var start_y;
var cursors = ['pen.png', 'eraser.png'];
var mode = "pen";
canvas.style.cursor = 'url("' + cursors[0] + '"), default';
document.getElementById("font-size").value = 12;
document.getElementById("font-family").value = 'arial';
let isdrawing = false;
let last = [];
let recycle = [];
var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
last.push(imgData);

canvas.addEventListener("mousedown", function(e) {
    x1 = e.offsetX + 5;
    y1 = e.offsetY + 30;
    isdrawing = true;
    if (mode == 'text') {
        text = '';
        textX = x1 - 5;
        textY = y1 - 30;
        addText(textX, textY);
    } else if (mode == 'rectangle' || mode == 'circle' || mode == 'triangle') {
        start_x = x1 - 5;
        start_y = y1 - 30;
    }
})

canvas.addEventListener("mousemove", function(e) {
    if (isdrawing) {
        if (mode == 'pen' || mode == 'eraser') {
            // 取得終點座標
            x2 = e.offsetX + 5;
            y2 = e.offsetY + 30;
            // 開始繪圖      
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.stroke();
            // 更新起始點座標
            x1 = x2;
            y1 = y2;
        } else if (mode == 'rectangle') {
            var now_x = e.clientX - canvas.offsetLeft;
            var now_y = e.clientY - canvas.offsetTop + 330;
            ctx.putImageData(last[last.length - 1], 0, 0);
            ctx.strokeRect(start_x, start_y, now_x - start_x, now_y - start_y);
        } else if (mode == 'circle') {
            var now_x = e.x - start_x;
            var now_y = e.y - start_y;
            ctx.beginPath();
            ctx.putImageData(last[last.length - 1], 0, 0);
            ctx.arc(start_x, start_y, Math.sqrt(now_x * now_x + now_y * now_y), 0, 2 * Math.PI);
            ctx.stroke();
        } else if (mode == 'triangle') {
            ctx.beginPath();
            ctx.putImageData(last[last.length - 1], 0, 0);
            ctx.moveTo((start_x + e.x - 57) / 2, start_y - 100);
            ctx.lineTo(start_x, e.y + 20);
            ctx.lineTo(e.x - 57, e.y + 20);
            ctx.closePath();
            ctx.stroke();
        }
    }
})

canvas.addEventListener("mouseup", function(e) {
    isdrawing = false;
    last.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
})

function cursor1() {
    mode = 'pen';
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function pen() {
    mode = 'pen';
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
    ctx.strokeStyle = 'black';
}

function eraser() {
    mode = 'eraser';
    canvas.style.cursor = 'url("' + cursors[1] + '"), default';
    ctx.strokeStyle = canvas.style.background; //跟隨畫布底色
}

function textMode() {
    mode = 'text';
    canvas.style.cursor = 'text';
}

function addText(x, y) {
    var inputText = document.createElement("input");
    inputText.id = "inputText";
    inputText.style.position = "absolute";
    inputText.style.left = `${x}px`;
    inputText.style.top = `${y + 175}px`;
    var canvasArea = document.getElementById("canvasArea")
    canvasArea.appendChild(inputText);
    inputText.addEventListener("keydown", function(event) {
        if (event.key == 'Enter') {
            var text = inputText.value;
            canvasArea.removeChild(inputText);
            var fontSize = document.getElementById("font-size").value;
            var fontFamily = document.getElementById("font-family").value;
            ctx.font = `${fontSize}px ${fontFamily}`;
            ctx.fillText(text, x, y + 5);
            last.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            recycle.splice(0, recycle.length);
        }
    })
}

function rectangleMode() {
    mode = 'rectangle';
    canvas.style.cursor = 'crosshair';
}

function circleMode() {
    mode = 'circle';
    canvas.style.cursor = 'crosshair';
}

function triangleMode() {
    mode = 'triangle';
    canvas.style.cursor = 'crosshair';
}

function undo() {
    if (last.length > 1) {
        recycle.push(last[last.length - 1]);
        last.pop();
        ctx.putImageData(last[last.length - 1], 0, 0);
    }
}

function redo() {
    if (recycle.length > 0) {
        last.push(recycle[recycle.length - 1]);
        recycle.pop();
        ctx.putImageData(last[last.length - 1], 0, 0);
    }
}

function download() {
    var lnk = document.createElement('a'),
        e;
    lnk.download = 'myImage';
    lnk.href = canvas.toDataURL("image/png;base64");
    if (document.createEvent) {
        e = document.createEvent("MouseEvents");
        e.initMouseEvent("click", true, true, window,
            0, 0, 0, 0, 0, false, false, false,
            false, 0, null);
        lnk.dispatchEvent(e);
    } else if (lnk.fireEvent) {
        lnk.fireEvent("onclick");
    }
}

var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', upload, false);

function upload(e) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.src = event.target.result;
        img.onload = function() {
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        }
    }
    reader.readAsDataURL(e.target.files[0]);
    last.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    recycle.splice(0, recycle.length);
}

function reload() { window.location.reload(); }

function change(element) {
    mode = 'pen';
    ctx.strokeStyle = element.style.background;
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change2() {
    mode = 'pen';
    ctx.strokeStyle = "#FF0000";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change3() {
    mode = 'pen';
    ctx.strokeStyle = "#0000C6";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change4() {
    mode = 'pen';
    ctx.strokeStyle = "#00DB00";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change5() {
    mode = 'pen';
    ctx.strokeStyle = "#F9F900";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change6() {
    mode = 'pen';
    ctx.strokeStyle = "#FF0080";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change7() {
    mode = 'pen';
    ctx.strokeStyle = "#750075";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change8() {
    mode = 'pen';
    ctx.strokeStyle = "#00FFFF";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change9() {
    mode = 'pen';
    ctx.strokeStyle = "#1AFD9C";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change10() {
    mode = 'pen';
    ctx.strokeStyle = "#F75000";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change11() {
    mode = 'pen';
    ctx.strokeStyle = "#808040";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change12() {
    mode = 'pen';
    ctx.strokeStyle = "#13579B";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change13() {
    mode = 'pen';
    ctx.strokeStyle = "#B97531";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change14() {
    mode = 'pen';
    ctx.strokeStyle = "#02CE2F";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}

function change15() {
    mode = 'pen';
    ctx.strokeStyle = "#CF00C5";
    canvas.style.cursor = 'url("' + cursors[0] + '"), default';
}